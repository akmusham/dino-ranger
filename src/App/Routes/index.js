import React from 'react'
import MyComponent from '../../Components/MyComponent'
import LoginPage from '../../Components/LoginPage'
import RegisterPage from '../../Components/RegisterPage'
import { withRouter, Route, Switch } from "react-router-dom"
import UploadFiles from '../../Components/uploadFiles'

function Routes() {
  return(
    <Switch>
      <Route exact path="/" component={LoginPage} />
      <Route path="/register" component={RegisterPage} />
      <Route path="/user" component={MyComponent} />
      <Route path="/uploadFiles" component={UploadFiles} />
    </Switch>
  )
}

export default withRouter(Routes)
