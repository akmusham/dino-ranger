import React from 'react'
import Routes from './Routes/'
import { BrowserRouter, useHistory } from 'react-router-dom';

class App extends React.Component {
  render(){
    return(
      <BrowserRouter>
          <Routes history={useHistory} />
      </BrowserRouter>
    )
  }
}

export default App
