import React,{useState,useEffect} from 'react'
import axios from 'axios'
import {SERVER_URL} from '../config.js'
import {
  Link
} from "react-router-dom";


function LoginPage(props) {
  const [email,setEmail] = useState('');
  const [pass,setPass] = useState('')
  const _handleSubmit = async() => {
    let obj = {email,pass}
    try {
      let {data: {data}} = await axios.post(`${SERVER_URL}/dinoranger/api/v1/authLogin`,obj)
      localStorage.setItem('userData', JSON.stringify(data))
      props.history.push('/user')
    } catch (e) {
      console.log(e);
    }
  }

  useEffect(()=>{
    if (localStorage.getItem('userData')) {
      props.history.push('/user')
    }else {
      props.history.push('/')
    }
  },[])

  return(
    <div>
      <div className="form-container">
      <h3>Login</h3>
        <input className="input-wrapper" name="email" placeholder="email" value={email} onChange={(e) => setEmail(e.target.value)} />
        <input className="input-wrapper" name="pass" placeholder="password" value={pass} onChange={(e) => setPass(e.target.value)} />
        <button className="button-wrapper" onClick={()=> _handleSubmit()}>Submit</button>
        <Link to="/register">Register</Link>
      </div>
    </div>
  )
}

export default LoginPage
